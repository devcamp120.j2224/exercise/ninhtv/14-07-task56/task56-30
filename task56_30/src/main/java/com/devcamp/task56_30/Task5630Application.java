package com.devcamp.task56_30;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5630Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5630Application.class, args);
	}

}
