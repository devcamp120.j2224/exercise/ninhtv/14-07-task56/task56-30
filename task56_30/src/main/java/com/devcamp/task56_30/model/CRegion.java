package com.devcamp.task56_30.model;

public class CRegion {
    private String regionName;
    private int regionCode;
    public CRegion(){

    }
    public CRegion(String regionName, int regionCode){
        super();
        this.regionName = regionName;
        this.regionCode = regionCode;
    }
    public CRegion instance(){
        return new CRegion();
    }
    public String getRegionName() {
        return regionName;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    public int getRegionCode() {
        return regionCode;
    }
    public void setRegionCode(int regionCode) {
        this.regionCode = regionCode;
    }
    
}
